<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <h1>EDIT PRODUCT</h1>
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-block">
                                    <form action="<?php echo site_url(); ?>/products/updateProces" method="post"
                                        id="frm_new_product">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="hidden" name="id_pro" id="id_pro"
                                                    value="<?php echo $productEdit->id_pro; ?>">

                                                <div class="form-group">
                                                    <label class="col-form-label">Name</label>
                                                    <div>
                                                        <input type="text" name="name_pro"
                                                            value="<?php echo $productEdit->name_pro; ?>" id="name_pro"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Category</label>
                                                    <div>
                                                        <input type="text" name="category_pro"
                                                            value="<?php echo $productEdit->category_pro; ?>"
                                                            id="category_pro" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="ol-form-label">IVA</label>
                                                    <div>
                                                        <select name="iva_pro" id="iva_pro" class="form-control">
                                                            <option value="<?php echo $productEdit->iva_pro; ?>">
                                                                <?php echo $productEdit->iva_pro; ?>
                                                            </option>

                                                            <option value="5">0 %</option>
                                                            <option value="12">12 %</option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">Price</label>
                                                    <div>
                                                        <input type="number" class="form-control" name="price_pro"
                                                            value="<?php echo $productEdit->price_pro; ?>"
                                                            id="price_pro">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Stock</label>
                                                    <div>
                                                        <input type="number" name="stock_pro"
                                                            value="<?php echo $productEdit->stock_pro; ?>"
                                                            id="stock_pro" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Country</label>
                                                    <div>
                                                        <input type="text" name="country_pro"
                                                            value="<?php echo $productEdit->country_pro; ?>"
                                                            id="country_pro" class="form-control">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" name="button" class="btn btn-primary">
                                                        Editar
                                                    </button>
                                                    &nbsp;
                                                    <a href="<?php echo site_url(); ?>/products/index"
                                                        class="btn btn-danger">
                                                        Cancelar
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>