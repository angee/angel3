<?php $totalProducts = 0;
if ($productList) {
    $totalProducts = sizeof($productList);
}
?>


<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-body">

                    <div class="row">
                        <div class="col-md-7">
                            <legend>DASHBOARD</legend>

                            <br>

                            <div class="card" style="width: 18rem;">
                                <div class="card-body">
                                    <h5 class="card-title">
                                        <?php echo $totalProducts; ?>
                                    </h5>
                                    <p class="card-text">PRODUCTS</p>

                                </div>
                            </div>

                            <br>
                            <div class="card" style="width: 18rem;">
                                <div class="card-body">
                                    <h5 class="card-title">$
                                        <?php echo $totalMoney; ?>
                                    </h5>
                                    <p class="card-text">TOTAL MONEY</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <h3>LIST OF PRODUCT</h3>
                            <div class="table-responsive">
                                <?php if ($productList): ?>
                                    <table class="table table-bordered table-hover" id="tblProducts">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE</th>
                                                <th>STOCK</th>
                                                <th>PRECIO</th>
                                                <th>ACCIONES</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($productList as $p): ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $p->id_pro ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $p->name_pro ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $p->stock_pro ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $p->price_pro ?>
                                                    </td>

                                                    <td class="text-center">
                                                        <a href="<?php echo site_url(); ?>/products/edit/<?php echo $p->id_pro ?>"
                                                            title="Editar Cliente" style="color:blue;">Update
                                                            <i class="bi bi-pencil-square"></i>
                                                        </a>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <a href="<?php echo site_url(); ?>/products/delete/<?php echo $p->id_pro; ?>"
                                                            title="Eliminar Clientes"
                                                            onclick="return confirm('¿Está seguro de eliminar este registro?');"
                                                            style="color:red;">
                                                            <i class="bi bi-trash-fill"></i>Delete
                                                        </a>

                                                    </td>

                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php else: ?>
                                    <h1>No product in database</h1>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>PRODUCTS WITH LARGEST STOCK</h5>
                            <canvas id="barChart1" width="400%" height="300px"></canvas>
                        </div>
                        <div class="col-md-6">
                            <h5>PRODUCTS WITH LOW STOCK</h5>
                            <canvas id="barChart2" width="400%" height="300px"></canvas>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>% PRODUCTS BY CATEGORY</h5>
                            <canvas id="pieCategory" width="80%" height="100px"></canvas>
                        </div>
                        <div class="col-md-6">
                            <h5># PRODUCTS BY CATEGORY</h5>
                            <canvas id="barCategory" width="80%" height="90px"></canvas>
                        </div>
                    </div>


                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>% PRODUCTS BY COUNTRY</h5>
                            <canvas id="pieCountry" width="80%" height="100px"></canvas>
                        </div>
                        <div class="col-md-6">
                            <h5># PRODUCTS BY COUNTRY</h5>
                            <canvas id="barCountry" width="80%" height="90px"></canvas>
                        </div>
                    </div>


                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>% PRODUCTS BY IVA</h5>
                            <canvas id="pieIva" width="80%" height="100px"></canvas>
                        </div>
                        <div class="col-md-6">
                            <h5># PRODUCTS BY IVA</h5>
                            <canvas id="barIva" width="80%" height="90px"></canvas>
                        </div>
                    </div>


                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>% AVERAGE PRODUCTS BY CATEGORY</h5>
                            <canvas id="pieAverage" width="80%" height="100px"></canvas>
                        </div>
                        <div class="col-md-6">
                            <h5># AVERAGE PRODUCTS BY CATEGORY</h5>
                            <canvas id="barAverage" width="80%" height="90px"></canvas>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    $("#tblProducts").DataTable();
</script>

<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($largeStock): ?>
                                                <?php foreach ($largeStock as $product): ?>
                                                                '<?php echo $product->name_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($largeStock): ?>
                                                <?php foreach ($largeStock as $product): ?>
                                                                <?php echo $product->stock_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('barChart1').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'bar',
        data: datos,
        options: opciones
    });
</script>

<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($lowStock): ?>
                                            <?php foreach ($lowStock as $product): ?>
                                                    '<?php echo $product->name_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($largeStock): ?>
                                            <?php foreach ($lowStock as $lowStock): ?>
                                                        <?php echo $product->stock_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)'  // Color de la tercera barra
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('barChart2').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'bar',
        data: datos,
        options: opciones
    });
</script>



<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($productsByCategory): ?>
                                            <?php foreach ($productsByCategory as $product): ?>
                                                    '<?php echo $product->category_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productsByCategory): ?>
                                            <?php foreach ($productsByCategory as $product): ?>
                                                        <?php echo $product->total_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'  // Color de la tercera barra
            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('barCategory').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'bar',
        data: datos,
        options: opciones
    });
</script>



<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($productsByCategory): ?>
                                            <?php foreach ($productsByCategory as $product): ?>
                                                    '<?php echo $product->category_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productsByCategory): ?>
                                            <?php foreach ($productsByCategory as $product): ?>
                                                        <?php echo $product->porcentaje_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 

            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('pieCategory').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'pie',
        data: datos,
        options: opciones
    });
</script>



<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($productsByCountry): ?>
                                            <?php foreach ($productsByCountry as $product): ?>
                                                    '<?php echo $product->country_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productsByCountry): ?>
                                            <?php foreach ($productsByCountry as $product): ?>
                                                        <?php echo $product->total_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'  // Color de la tercera barra
            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('barCountry').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'bar',
        data: datos,
        options: opciones
    });
</script>

<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($productsByCountry): ?>
                                            <?php foreach ($productsByCountry as $product): ?>
                                                    '<?php echo $product->country_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productsByCountry): ?>
                                            <?php foreach ($productsByCountry as $product): ?>
                                                        <?php echo $product->porcentaje_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 

            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('pieCountry').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'pie',
        data: datos,
        options: opciones
    });
</script>

<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($productsByIva): ?>
                                            <?php foreach ($productsByIva as $product): ?>
                                                    '<?php echo $product->iva_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productsByIva): ?>
                                            <?php foreach ($productsByIva as $product): ?>
                                                        <?php echo $product->total_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'  // Color de la tercera barra
            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('barIva').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'bar',
        data: datos,
        options: opciones
    });
</script>

<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($productsByIva): ?>
                                            <?php foreach ($productsByIva as $product): ?>
                                                    '<?php echo $product->iva_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productsByIva): ?>
                                            <?php foreach ($productsByIva as $product): ?>
                                                        <?php echo $product->porcentaje_pro; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 

            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('pieIva').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'pie',
        data: datos,
        options: opciones
    });
</script>


<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($averageProduct): ?>
                                            <?php foreach ($averageProduct as $product): ?>
                                                    '<?php echo $product->category_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($averageProduct): ?>
                                            <?php foreach ($averageProduct as $product): ?>
                                                        <?php echo $product->promedio_price; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'  // Color de la tercera barra
            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('barAverage').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'bar',
        data: datos,
        options: opciones
    });
</script>

<script type="text/javascript">
    // Datos de ejemplo
    var datos = {
        labels: [
            <?php if ($averageProduct): ?>
                                            <?php foreach ($averageProduct as $product): ?>
                                                    '<?php echo $product->category_pro; ?>',
                <?php endforeach; ?>
                                <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($averageProduct): ?>
                                            <?php foreach ($averageProduct as $product): ?>
                                                        <?php echo $product->promedio_price; ?>,
                    <?php endforeach; ?>
                                <?php endif; ?>
            ], // Valores de las barras
            backgroundColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 

            ],
            borderColor: [
                '#5ea39f', // Color de la primera barra
                '#77bab7', // Color de la segunda barra
                '#90d1cf',
                '#a8e8e7',
                '#c1ffff',
                '#d8e8e8' 
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('pieAverage').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'pie',
        data: datos,
        options: opciones
    });
</script>
