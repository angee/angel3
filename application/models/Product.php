<?php

class Product extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    //Read all data of product table

    function getAll()
    {
        //para ordenar los datos
        $this->db->order_by("name_pro", "asc");

        $result = $this->db->get("product");

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }

    }


    //Aplicar JMeter para hacer prueba 
    function getTotalMoney()
    {
        $sql = "SELECT sum(price_pro*stock_pro) as totalMoney from product;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->row()->totalMoney;
        } else {
            return 0;
        }
    }


    //get products wiht high  or down stock
    function getByStock($order, $limit)
    {
        $sql = "select * FROM product ORDER BY stock_pro $order LIMIT $limit;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }


    function getTotalProductBYCategory($order)
    {
        $sql = "SELECT category_pro, COUNT(id_pro) as total_pro, round((COUNT(id_pro) / (SELECT COUNT(*) FROM product)) * 100, 2) AS porcentaje_pro FROM product GROUP BY category_pro ORDER BY total_pro $order; ";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }


    function getTotalProductBYCountry($order)
    {
        $sql = "SELECT country_pro, COUNT(id_pro) as total_pro, round((COUNT(id_pro) / (SELECT COUNT(*) FROM product)) * 100, 2) AS porcentaje_pro FROM product GROUP BY country_pro ORDER BY total_pro DESC LIMIT 7;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }


    function getTotalProductByIva($order)
    {
        $sql = "SELECT iva_pro, COUNT(id_pro) as total_pro, round((COUNT(id_pro) / (SELECT COUNT(*) FROM product)) * 100, 2) AS porcentaje_pro FROM product GROUP BY iva_pro ORDER BY total_pro DESC; ";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }


    function getAveragePriceCategory($order)
    {
        $sql = "SELECT category_pro, AVG(price_pro) AS promedio_price FROM product GROUP BY category_pro ORDER BY promedio_price $order; ";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }


    function insert($datas)
    {
        //CONSULTAR ACTIVE RECORD -> CodeIgniter Inyeccion SQL
        return $this->db->insert("product", $datas);
    }

    function delete($id_pro)
    {
        $this->db->where("id_pro", $id_pro);
        return $this->db->delete("product");


    }

    function getById($id_pro)
    {
        $this->db->where("id_pro", $id_pro);
        $instructor = $this->db->get("product");
        if ($instructor->num_rows() > 0) {
            return $instructor->row();
        }
        return false;
    }

    function update($id_pro, $datas)
    {
        $this->db->where("id_pro", $id_pro);
        return $this->db->update('product', $datas);
    }





} //The class end
?>