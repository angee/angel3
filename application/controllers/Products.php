<?php
class Products extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //cargar modelo
        $this->load->model('Product');

    }
    //funcion que renderiza la vista index
    public function index()
    {
        $data['productList'] = $this->Product->getAll();
        $data['totalMoney'] = $this->Product->getTotalMoney();

        //REutilice la funcion getByStock pasando diferentes parametros
        $data['largeStock'] = $this->Product->getByStock("desc", 5);
        $data['lowStock'] = $this->Product->getByStock("asc", 5);

        $data['productsByCategory'] = $this->Product->getTotalProductBYCategory("desc");
        $data['productsByCountry'] = $this->Product->getTotalProductBYCountry("desc");
        $data['productsByIva'] = $this->Product->getTotalProductByIva("desc");
        $data['averageProduct'] = $this->Product->getAveragePriceCategory("desc");


        //PAra mostrar todos los datos
        // print_r($data);

        $this->load->view('header');
        $this->load->view('products/index', $data);
        $this->load->view('footer');

    }


    public function new()
    {
        $this->load->view('header');
        $this->load->view('products/new');
        $this->load->view('footer');
    }

    public function insert()
    {
        //codigo neto
        $dataNewProduct = array(
            "name_pro" => $this->input->post('name_pro'),
            "category_pro" => $this->input->post('category_pro'),
            "iva_pro" => $this->input->post('iva_pro'),
            "price_pro" => $this->input->post('price_pro'),
            "stock_pro" => $this->input->post('stock_pro'),
            "country_pro" => $this->input->post('country_pro'),
        );
        if ($this->Product->insert($dataNewProduct)) {
            redirect('products/index');
        } else {
            echo "<h1>ERROR INSERT</h1>";
        }
    }





    public function delete($id_pro)
    {
        if ($this->Product->delete($id_pro)) { //invocando el modelo
            redirect('products/index');
        } else {
            echo "ERROR DELETE :()";
        }

    }


    public function edit($id_pro)
    {
        $data["productEdit"] = $this->Product->getById($id_pro);
        $this->load->view('header');
        $this->load->view('products/edit', $data);
        $this->load->view('footer');
    }


    public function updateProces($value = '')
    {
        $dataEdit = array(
            "name_pro" => $this->input->post('name_pro'),
            "category_pro" => $this->input->post('category_pro'),
            "iva_pro" => $this->input->post('iva_pro'),
            "price_pro" => $this->input->post('price_pro'),
            "stock_pro" => $this->input->post('stock_pro'),
            "country_pro" => $this->input->post('country_pro'),
        );
        $id_pro = $this->input->post("id_pro");
        if ($this->Product->update($id_pro, $dataEdit)) {
            redirect("products/index");
        } else {
            echo "ERROR DELETE :()";
        }

    }


} //The class end
?>
